import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'
import CharacterCard from '../components/CharacterCard'

const axios = require('axios')

const Episode = () => {
    const [episode, setEpisode] = useState(null)
    const { id } = useParams()

    useEffect(() => {
        axios.get(`https://rickandmortyapi.com/graphql?query={episode(id: ${id}){name episode characters{image name species}}}`)
            .then(response => {
                setEpisode(response.data.data.episode)
            })
    }, [])

    return (
        episode ?
            <div>
                <h3>{episode.name}</h3>
                <h5>{episode.episode}</h5>
                <div className="rick-and-morty-gallery">
                    {episode.characters.map((character, i) => <CharacterCard key={i} character={character} />)}
                </div>
            </div>
            : <p>chargement en cours</p>
    )
}

export default Episode