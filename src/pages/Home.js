import React, { useEffect, useState } from 'react';
import RickAndMortyEpisodeCard from '../components/RickAndMortyEpisodeCard';
import SearchBar from '../components/SearchBar'

const axios = require('axios')

const Home = () => {
    const [episodes, setEpisodes] = useState([])
    const [episodeName, setEpisodeName] = useState('')

    var episodesArray = []

    const callApi = function (page = 1) {
        axios.get(`https://rickandmortyapi.com/graphql?query={episodes(page:${page} filter:{name: "${episodeName}"}){info{next}results{id name episode}}}`)
            .then(response => {
                episodesArray = episodesArray.concat(response.data.data.episodes.results)
                page = response.data.data.episodes.info.next

                if (page) callApi(page)
                else setEpisodes(episodesArray)
            })
    }

    const onSubmit = event => {
        event.preventDefault()
        callApi()
    }


    useEffect(() => {
        callApi()
    }, [])

    return (
        <div>
            <SearchBar onSubmit={onSubmit} setEpisodeName={setEpisodeName} />

            <div className="rick-and-morty-gallery">
                {episodes.map((episode, i) => <RickAndMortyEpisodeCard key={i} episode={episode} />)}
            </div>
        </div>
    )
}

export default Home