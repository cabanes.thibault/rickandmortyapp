import React from 'react'

const SearchBar = ({ setEpisodeName, onSubmit }) => {
    const onChange = event => {
        setEpisodeName(event.target.value)
    }

    return (
        <form className="search-bar" onSubmit={onSubmit}>
            <label htmlFor="name">Search episodes by name</label>
            <input name="name" type="text" placeholder="Episode name" onChange={onChange} />
            <input type="submit" value="Search" />
        </form>
    )
}

export default SearchBar