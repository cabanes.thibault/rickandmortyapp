import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import selectedFav from '../selectedFav.svg'
import unSelectedFav from '../unSelectedFav.svg'

const RickAndMortyEpisodeCard = ({ episode }) => {
    const [favori, setFavori] = useState(false)


    const onClick = () => {
        if (favori) {
            let favoris = window.localStorage.getItem('favoris').split(',')
            favoris.splice(favoris.indexOf(episode.id), 1)
            window.localStorage.setItem('favoris', favoris.join(','))
        }
        else {
            let favoris = window.localStorage.getItem('favoris') ? window.localStorage.getItem('favoris').split(',') : []
            favoris.push(episode.id)
            window.localStorage.setItem('favoris', favoris.join(','))
        }

        setFavori(!favori)
    }

    useEffect(() => {
        let favoris = window.localStorage.getItem('favoris') ? window.localStorage.getItem('favoris').split(',') : []

        if (favoris.includes(episode.id)) {
            setFavori(true)
        }
    }, [])

    return (
        <div className="rick-and-morty-card" >
            <img className="favoris" src={favori ? selectedFav : unSelectedFav} onClick={onClick} />
            <h3>{episode.name}</h3>
            <h5>{episode.episode}</h5>
            <Link to={`/episode/${episode.id}`}>see</Link>
        </div>
    )
}

export default RickAndMortyEpisodeCard