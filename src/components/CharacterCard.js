import React from 'react'

const CharacterCard = ({ character }) => {
    return (
        <div className="character-card">
            <img src={character.image} alt="character" />
            <h4>{character.name}</h4>
            <p>species: {character.species} </p>
        </div>
    )
}

export default CharacterCard