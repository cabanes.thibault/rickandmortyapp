import './App.css';
import Home from './pages/Home';
import Episode from './pages/Episode';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <header className="App-header">
          <Link to="/" className="home-link">Rick & Morty App</Link>
        </header>

        <Switch>
          <Route exact path="/" render={() => <Home />} />
          <Route path="/episode/:id" render={() => <Episode />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
