# RickAndMortyApp

A rick and morty app

You need to install npm to launch the application

To start the development server, run `npm start`


Réponses:

1. Décrivez l’intérêt de React par rapport à du JS / HTML classique (par ex. jQuery) à quelqu’un qui n’y connaît rien en développement informatique. 

	En développement d'applications web, on utilisait historiquement du code html pour
structurer une page web et son affichage, du css pour gérer le style graphique des pages web et
du javascript pour gérer les interractions des utilisateurs avec les élèments de la page web
(évènement au clique sur un boutton, au survol d'un élèment etc...). Il fallait donc au moins
trois fichiers, un fichier html avec tous les élèments de la page, un fichier css et un fichier
js, ce qui rendais la lecture du code un peu difficile et qui limitait (à moins d'utiliser du
php) la réemployabilité des élèments html (pour une navbar, il fallait recopier le code html
sur toutes les pages où on voulait la voir apparaitre). De plus, chaque page et les fichiers
qui lui sont liés faisaient l'objet d'une requête vers un serveur distant, ce qui pouvait
rendre le changement d'une page vers une autre assez long.

	L'arrivée de React à permis de rendre le code plus lisible et réemployable (Pour suivre
l'exemple précédent, plutôt que de recopier la navbar sur toutes les pages html, on peut
maintenant créer un composant navbar puis appeler ce composant sur les pages où l'on veux le
voir apparaitre), de rendre la gestion des interractions utilisateurs plus simples (En
javascript, il fallait selectionner les élèments html par identifiant, classe ou attributs,
pour y attacher des évènements. Grâce à React, on peut directemment attacher des évènements aux
élèments sans devoir les selectionner avant), et surtout de rendre le changement des pages
beaucoup plus fluide (Une application web en React envoie une requête au serveur pour récupérer
le code, puis gère ensuite l'affichage des différents contenus sans recharger une nouvelle
page). L'inconvénient majeur de l'utilisation de React est le réfèrencement du site qui est 
moins évident.




2. En PHP, et de manière générale en programmation orientée objet, à quoi servent les interfaces ? 

    En POO, une interface permet de déclarer le nom des méthodes publiques (ainsi que les variables
passées à ces méthodes) qui doivent exister sur les classes qui l'implémentent, sans devoir
définir leur fonctionnement. Ceci permet de s'assurer qu'une méthode, quelque soit son
fonctionnement, soit bien présente sur toutes les instances des classes qui en implémente
l'interface et qu'elle puisse être appelé en dehors du scope de la classe (this).




3. Bill Gates a dit “Je recrute toujours les développeurs les plus fainéants possible, car je sais qu’il produiront du code qui offre un maximum de résultat pour un minimum d’effort”. Qu’en pensez-vous ? 

    Je pense que c'est vrai, comme mentionné dans la réponse à la question 1, la réemployabilité
est une des idées les plus efficaces en développement, un développeur fainéant chercheras un
moyen de rendre son code efficace et réemployable, pour diminuer le nombre de lignes de code
nécessaire, afin de ne pas copier coller un même code dans plusieurs fichiers, ceci a un
avantage majeur en terme de maintenance, puisque le code réemployable ne doit être 
amélioré/corrigé que dans un seul fichier, contrairement au 'copier coller' qui nécessiteras
une maintenance dans tous les fichiers où le code a été dupliqué. Ainsi le développeur fainéant
réflèchiras toujours à une manière de factoriser son code